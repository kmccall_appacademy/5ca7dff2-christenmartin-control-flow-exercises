# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
 str.each_char {|ch| str.delete!(ch.downcase)}
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  middle = str.length / 2

  if str.length.even?
    str[middle-1 .. middle]
  else
    str[middle]
  end
end

# Return the number of vowels in a string.

def num_vowels(str)
  vowels = ["a", "e", "i", "o", "u"]
  count = 0

  str.split("").each {|c| count +=1 if vowels.include?(c)}

  count
end


# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1

  (2..num).each do |increment_by_one|
    product *= increment_by_one
  end

  product
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""

  arr.each do |word|
    joined += word
    joined += separator unless word == arr.last
  end

  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"

def weirdcase(str)
  str.split("")
  .map
  .each_with_index do |char, idx|
    if idx.even?
      char.downcase
    else
      char.upcase
    end
  end
  .join("")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split

  words.each_with_index do |word, idx|
    if word.length >= 5
      words[idx] = word.split("").reverse.join("")
    end
  end

  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzzed = []

  (1..n).each do |num|
    if num % 3 == 0 && num % 5 == 0
      fizzbuzzed << "fizzbuzz"
    elsif num % 3 === 0
      fizzbuzzed << "fizz"
    elsif num % 5 === 0
      fizzbuzzed << "buzz"
    else
      fizzbuzzed << num
    end
  end

  fizzbuzzed
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed = []

  arr.each { |word| reversed.unshift(word) }

  reversed
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  elsif num == 2
    return true
  end

  (2...num).any? { |other_num| return false if num % other_num == 0 }

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_nums = []

  (1..num).each { |num2| factor_nums << num2 if num % num2 == 0 }

  factor_nums.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = factors(num)

  factors.select { |num| prime?(num) }.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = arr.select { |num| num.even? }
  odds = arr.select { |num| num.odd? }

  if evens.length == 1
    evens[0]
  else
    odds[0]
  end
end
